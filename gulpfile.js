var gulp = require("gulp");
var concat = require("gulp-concat");
var sourcemaps = require("gulp-sourcemaps");
var sass = require("gulp-sass");
var environments = require("gulp-environments");
var cleanCSS = require("gulp-clean-css");
var angularFilesort = require("gulp-angular-filesort");
var ngAnnotate = require("gulp-ng-annotate");
var removeLogging = require("gulp-remove-logging");
var runSequence = require("run-sequence");
var browserSync = require("browser-sync").create();
var gutil = require("gulp-util");

var development = environments.development;
var production = environments.production;

gulp.task("build:vendor", function () {
    return gulp.src([
        "bower_components/angular/angular.js",
        "bower_components/angular-ui-router/release/angular-ui-router.js",
        "bower_components/angular-messages/angular-messages.js ",
        "bower_components/ngstorage/ngStorage.js",
        "bower_components/angular-animate/angular-animate.js",
        "bower_components/angular-ui-grid/ui-grid.js",
        "bower_components/bootstrap-select/dist/js/bootstrap-select.js",
        "bower_components/ng-bootstrap-select/build/ng-bootstrap-select.js",
        "bower_components/ng-file-upload/ng-file-upload.js"
    ])

        .pipe(development(sourcemaps.init()))
        .pipe(concat("vendor.js"))
        //.pipe(uglify())
        .on("error", function (err) {
            gutil.log(gutil.colors.red("[Error]"), err.toString())
        })
        .pipe(development(sourcemaps.write()))
        .pipe(gulp.dest("./dist"))
});

gulp.task("build:sass", function () {
    return gulp.src([
        "src/**/*.scss",
        "bower_components/angular-ui-grid/ui-grid.css",
        "bower_components/bootstrap-select/dist/css/bootstrap-select.css"
    ])
        .pipe(sass())
        .pipe(development(sourcemaps.init()))
        .pipe(concat("bundle.css"))
        .pipe(production(cleanCSS()))
        .pipe(development(sourcemaps.write()))
        .pipe(gulp.dest("./dist"))
        .pipe(development(browserSync.stream()));
});

gulp.task("build:js", function () {
    return gulp.src([
        "src/**/*.js",
        "!src/**/*.spec.js",
    ])
        .pipe(angularFilesort())
        .pipe(development(sourcemaps.init()))
        .pipe(concat("bundle.js"))
        .pipe(production(ngAnnotate()))
        .pipe(production(removeLogging()))
        //.pipe(production(uglify()))
        .pipe(development(sourcemaps.write()))
        .pipe(gulp.dest("./dist"))
});


gulp.task("build:other", function () {
    return gulp.src([
        "src/**/*",
        "!src/**/*.js",
        "!src/**/*.sass"
    ])
        .pipe(gulp.dest("./dist"))
});

gulp.task("build", function (done) {
    runSequence(["build:js",
        "build:vendor",
        "build:sass",
        "build:other"], done);
});

gulp.task("serve", ["build"], function () {
    browserSync.init({
        host: "0.0.0.0",
        server: {
            baseDir: "./dist"
        }
    });

    gulp.watch([
        "src/**/*.scss"
    ], [
        "build:sass"
    ]);

    gulp.watch([
        "src/**/*.js",
        "!src/**/*.spec.js"
    ], [
        "build:js"
    ]).on("change", browserSync.reload);


    gulp.watch([
        "src/**/*",
        "!src/**/*.js",
        "src/**/*.scss"
    ], [
        "build:other"
    ]).on("change", browserSync.reload);
});
