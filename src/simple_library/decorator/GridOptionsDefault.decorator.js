angular.module("simple_library")
    .config(function ($provide) {
        $provide.decorator("GridOptions", function ($delegate) {
            var gridOptions = angular.copy($delegate);

            gridOptions.initialize = function (options) {
                var initOptions = $delegate.initialize(options);
                initOptions.enableColumnMenus = false;
                initOptions.useExternalPagination = true;
                initOptions.paginationPageSizes = [10, 15, 20];
                return initOptions;
            };

            return gridOptions;
        })
    });