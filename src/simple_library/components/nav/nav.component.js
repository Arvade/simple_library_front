angular.module("simple_library")
    .component("navigation", {
        templateUrl: "simple_library/components/nav/nav.component.html",
        controller: "NavController",
        controllerAs: "$ctrl"
    });