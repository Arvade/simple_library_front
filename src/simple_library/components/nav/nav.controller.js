angular.module("simple_library")
    .controller("NavController", ["$rootScope", "$state", "SessionService", "LoggingService",
        function ($rootScope, $state, SessionService, LoggingService) {
            var $ctrl = this;
            $ctrl.isLoggedIn = false;
            $ctrl.username = "none";
            $ctrl.$onInit = update;
            $ctrl.logout = logout;
            $rootScope.$on("SessionService:logged", update);
            $rootScope.$on("SessionService:logout", update);

            $ctrl.states = {
                books: {
                    available: ["public.home", "app.home"],
                    current: "public.home"
                },
                brand: {
                    available: ["public.home", "app.home"],
                    current: "public.home"
                }
            };

            function logout() {
                SessionService.logout();
                $state.go("public.home");
                $ctrl.isLoggedIn = false;
                LoggingService.success("Logout success.");
            }

            function update() {
                $ctrl.isLoggedIn = SessionService.isLoggedIn();
                if ($ctrl.isLoggedIn) {
                    $ctrl.username = SessionService.getUsername();
                }
                $ctrl.states.books.current = $ctrl.states.books.available[+$ctrl.isLoggedIn];
                $ctrl.states.brand.current = $ctrl.states.brand.available[+$ctrl.isLoggedIn];
            }
        }]);