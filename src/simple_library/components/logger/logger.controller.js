angular.module("simple_library")
    .controller("LoggerController", ["$timeout", "$rootScope",
        function ($timeout, $rootScope) {
        var $ctrl = this;

        $ctrl.logs = [];
        $ctrl.hasLog = false;
        $ctrl.pushLog = pushLog;

        function pushLog(type, content) {
            var log = {
                type: type,
                content: content
            };

            $ctrl.logs.push(log);
            $ctrl.hasLog = true;

            $timeout(function () {
                var toRemove = $ctrl.logs.indexOf(log);
                if (toRemove > -1) {
                    $ctrl.logs.splice(toRemove, 1);
                }
                if ($ctrl.logs.length <= 0) {
                    $ctrl.hasLog = false;
                }
            }, 4000);
        }

        var deregister = $rootScope.$on("LoggingService:log-added", function logAdded(event, data) {
            $ctrl.pushLog(data.type, data.content);
        });

        $rootScope.$on("$destroy", function destroyScope() {
            deregister();
        })
    }]);