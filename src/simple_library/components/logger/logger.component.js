angular.module("simple_library")
    .component("logger", {
        templateUrl: "simple_library/components/logger/logger.component.html",
        controller: "LoggerController",
        controllerAs: "$ctrl"
    });