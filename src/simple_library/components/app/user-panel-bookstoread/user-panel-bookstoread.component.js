angular.module("simple_library")
    .component("userPanelBookstoread", {
        templateUrl: "simple_library/components/app/user-panel-bookstoread/user-panel-bookstoread.component.html",
        controller: "UserPanelBooksToReadController",
        controllerAs: "$ctrl",
        bindings: {
            toReadBooks:"<"
        }
    });