angular.module("simple_library")
    .component("userPanelAddcategory", {
        templateUrl: "simple_library/components/app/user-panel-addcategory/user-panel-addcategory.component.html",
        controller: "AddCategoryController",
        controllerAs: "$ctrl"
    });