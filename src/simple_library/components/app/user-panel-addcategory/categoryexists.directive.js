angular.module("simple_library")
    .directive("categoryExists", ["$q", "$http", "REST_BASEURL", "CategoryService",
        function ($q, $http, REST_BASEURL, CategoryService) {
        return {
            require: "ngModel",
            link: function (scope, element, attrs, ngModel) {
                ngModel.$asyncValidators.categoryExists = function (modelValue, viewValue) {
                    if (viewValue == null || viewValue.length === 0) return;
                    var category = {
                        name: viewValue
                    };

                    return CategoryService.categoryExists(category)
                        .then(function (data) {
                            var exists = data.exists;
                            if (exists) {
                                return $q.reject();
                            }
                            return true;
                        });
                }
            }
        }
    }]);