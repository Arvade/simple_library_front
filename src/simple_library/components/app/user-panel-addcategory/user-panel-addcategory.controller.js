angular.module("simple_library")
    .controller("AddCategoryController", ["CategoryService", "LoggingService",
        function (CategoryService, LoggingService) {
            var $ctrl = this;
            $ctrl.submit = submit;
            $ctrl.category = {
                name: ""
            };

            function submit() {
                CategoryService.addCategory($ctrl.category)
                    .then(function () {
                        LoggingService.success("Category added");
                        reset();
                    }, function () {
                        LoggingService.error("Could not add category");
                    });
            }

            function reset(){
                $ctrl.category = {};
                $ctrl.addCategoryForm.$setUntouched();
                $ctrl.addCategoryForm.$setPristine();
            }
        }]);