angular.module("simple_library")
    .component("app", {
        templateUrl: "simple_library/components/app/app.component.html",
        controller: "AppController",
        controllerAs: "$ctrl"
    });