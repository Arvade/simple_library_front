angular.module("simple_library")
    .controller("AddBookController", ["BookService", "LoggingService", "FileService",
        function (BookService, LoggingService, FileService) {
            var $ctrl = this;
            $ctrl.submit = submit;
            $ctrl.reset = reset;
            $ctrl.uploadCoverImage = uploadCoverImage;
            $ctrl.uploadBookContent = uploadBookContent;
            $ctrl.defaultBook = {
                title: "",
                description: "",
                categories: {},
                coverImageId: null,
                contentId: null
            };

            $ctrl.book = {
                title: "",
                description: "",
                categories: {},
                coverImageId: null,
                contentId: null
            };

            function submit() {
                BookService.addBook($ctrl.book)
                    .then(function () {
                        LoggingService.success("Book added");
                        reset();
                    }, function () {
                        LoggingService.error("Could not add book");
                    });
            }

            function uploadBookContent(file) {
                uploadFile(file)
                    .then(function (id) {
                        $ctrl.book.contentId = id;
                    });
            }

            function uploadCoverImage(file) {
                uploadFile(file)
                    .then(function (id) {
                        $ctrl.book.coverImageId = id;
                    });
            }

            function uploadFile(file) {
                return FileService.uploadFile(file)
                    .then(function (data) {
                        LoggingService.success("File uploaded");
                        return data.id;
                    }, function () {
                        LoggingService.error("Could not upload file");
                    });
            }

            function reset() {
                $ctrl.book = $ctrl.defaultBook;
                $ctrl.addBookForm.$setUntouched();
                $ctrl.addBookForm.$setPristine();
            }
        }]);