angular.module("simple_library")
    .directive("fileRead", [function () {
        return {
            scope: {
                fileRead: "="
            },
            link: function (scope, element, attrs) {
                element.bind("change", function (changeEvent) {
                    var reader = new FileReader();
                    reader.onload = function (loadEvent) {
                        scope.$apply(function () {
                            scope.fileRead = loadEvent.target.result;
                        });
                    };
                    reader.readAsDataURL(changeEvent.target.files[0]);
                })
            }
        }
    }]);