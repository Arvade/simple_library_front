angular.module("simple_library")
    .component("userPanelAddbook", {
        templateUrl: "simple_library/components/app/user-panel-addbook/user-panel-addbook.component.html",
        controller: "AddBookController",
        controllerAs: "$ctrl",
        bindings:{
            categories:"<"
        }
    });