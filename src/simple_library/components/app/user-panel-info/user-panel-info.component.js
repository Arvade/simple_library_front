angular.module("simple_library")
    .component("userPanelInfo", {
        templateUrl: "simple_library/components/app/user-panel-info/user-panel-info.component.html",
        controller: "UserPanelInfoController",
        controllerAs: "$ctrl",
        bindings: {
            userInfo: "<"
        }
    });