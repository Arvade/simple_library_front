angular.module("simple_library")
    .controller("UserListController", ["$scope", "$state", "UserService", "LoggingService", "$stateParams",
        function ($scope, $state, UserService, LoggingService) {
            $scope.paginationOptions = {
                page: 1,
                size: 10
            };

            $scope.gridOptions = {
                paginationPageSize: $scope.paginationOptions.size,
                columnDefs: [
                    {
                        name: 'id',
                        enableCellEdit: false,
                        width: "5%"
                    },
                    {name: 'username'},
                    {name: 'firstname'},
                    {name: 'lastname'},
                    {name: 'email'},
                    {
                        name: "enabled",
                        type: "boolean",
                        cellFilter: "mapEnabled",
                        editDropdownValueLabel: "enabled",
                        editableCellTemplate: "ui-grid/dropdownEditor",
                        editDropdownOptionsArray: [
                            {id: true, enabled: "enabled"},
                            {id: false, enabled: "disabled"}
                        ],
                        width: "10%"
                    },
                    {
                        name: "authorities",
                        displayName: "Roles",
                        editDropdownValueLabel: "role",
                        editableCellTemplate: "ui-grid/dropdownEditor",
                        editDropdownOptionsArray: [
                            {id: "ROLE_USER", role: "USER"},
                            {id: "ROLE_ADMIN", role: "ADMIN"}
                        ],
                        cellFilter: "mapRoles"
                    }
                ],
                onRegisterApi: onRegisterApi
            };

            UserService.getUserList($scope.paginationOptions)
                .then(saveData);

            function onRegisterApi(gridApi) {
                $scope.gridApi = gridApi;
                gridApi.pagination.on.paginationChanged($scope,
                    function (pageNumber, pageSize) {
                        $scope.paginationOptions.page = pageNumber;
                        $scope.paginationOptions.size = pageSize;

                        UserService.getUserList($scope.paginationOptions)
                            .then(saveData);

                        $state.go(".", {page: pageNumber, size: pageSize}, {notify: false});
                    });

                gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
                    if (newValue === oldValue) return;
                    var updateRequest = {
                        id: rowEntity.id,
                        field: colDef.name,
                        newValue: newValue
                    };

                    UserService.updateUser(updateRequest)
                        .then(function () {
                            LoggingService.success("User updated");
                        }, function () {
                            rowEntity[colDef.name] = oldValue;
                            LoggingService.error("Could not update user");
                        });
                });
            }

            function saveData(data) {
                $scope.gridOptions.data = data.content;
                $scope.gridOptions.totalItems = data.totalElements;
            }
        }]);