angular.module("simple_library")
    .component("userPanelUserlist", {
        templateUrl: "simple_library/components/app/user-panel-userlist/user-panel-userlist.component.html",
        controller: "UserListController",
        controllerAs: "$ctrl",
        bindings:{
            users:"<"
        }
    });