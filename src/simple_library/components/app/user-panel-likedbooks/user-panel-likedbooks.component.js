angular.module("simple_library")
    .component("userPanelLikedbooks", {
        templateUrl: "simple_library/components/app/user-panel-likedbooks/user-panel-likedbooks.component.html",
        controller: "UserPanelLikedBooksController",
        controllerAs: "$ctrl",
        bindings:{
            likedBooks:"<"
        }
    });