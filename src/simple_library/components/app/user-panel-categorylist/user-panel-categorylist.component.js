angular.module("simple_library")
    .component("userPanelCategorylist", {
        templateUrl: "simple_library/components/app/user-panel-categorylist/user-panel-categorylist.component.html",
        controller: "CategoryListController",
        controllerAs: "$ctrl"
    });