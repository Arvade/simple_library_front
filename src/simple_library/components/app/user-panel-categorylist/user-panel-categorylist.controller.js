angular.module("simple_library")
    .controller("CategoryListController", ["$scope", "CategoryService", "LoggingService",
        function ($scope, CategoryService, LoggingService) {
            $scope.remove = remove;
            $scope.paginationOptions = {
                page: 1,
                size: 10
            };

            $scope.gridOptions = {
                paginationPageSize: $scope.paginationOptions.size,
                columnDefs: [
                    {
                        name: "id",
                        enableCellEdit: false,
                        width: "5%"
                    },
                    {
                        name: "name"
                    },
                    {
                        field: " ",
                        cellTemplate: "<button class='btn btn-sm btn-danger' ng-disabled='row.entity.disabled' ng-click='grid.appScope.remove(row.entity)'>Remove</button>",
                        enableCellEdit: false,
                        width: "10%"
                    }
                ],
                onRegisterApi: onRegisterApi
            };

            CategoryService.getBookCategoriesPaginated($scope.paginationOptions)
                .then(saveData);

            function remove(entity) {
                var id = entity.id;
                entity.disabled = true;
                CategoryService.removeCategory(id)
                    .then(function () {
                        LoggingService.success("Category removed");
                        removeCategoryFromGridDataById(id);
                    }, function () {
                        entity.disable = false;
                        LoggingService.error("Category could not be removed");
                    });
            }

            function removeCategoryFromGridDataById(id) {
                var data = $scope.gridOptions.data;
                for (var index = 0; index < data.length; index++) {
                    var row = data[index];
                    if (row.id === id) {
                        data.splice(index, 1);
                    }
                }
            }

            function onRegisterApi(gridApi) {
                $scope.gridApi = gridApi;
                gridApi.pagination.on.paginationChanged($scope,
                    function (pageNumber, pageSize) {
                        $scope.paginationOptions.page = pageNumber;
                        $scope.paginationOptions.size = pageSize;

                        CategoryService.getBookCategoriesPaginated($scope.paginationOptions)
                            .then(saveData);
                    });

                gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
                    if (newValue === oldValue) return;
                    var updateRequest = {
                        id: rowEntity.id,
                        name: newValue
                    };

                    CategoryService.updateCategory(updateRequest)
                        .then(function () {
                            LoggingService.success("Category updated");
                        }, function () {
                            rowEntity[colDef.name] = oldValue;
                            LoggingService.error("Could not update category");
                        });
                });
            }

            function saveData(data) {
                appendIsDisabledFlag(data);
                $scope.gridOptions.data = data.content;
                $scope.gridOptions.totalItems = data.totalElement;
            }

            function appendIsDisabledFlag(data) {
                angular.forEach(data, function (row) {
                    row.disabled = false;
                });
            }
        }]);