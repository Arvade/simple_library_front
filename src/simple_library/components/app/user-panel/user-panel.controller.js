angular.module("simple_library")
    .controller("UserPanelController", ["UserService", function (UserService) {
        var $ctrl = this;
        $ctrl.isAdmin = UserService.isAdmin();
    }]);