angular.module("simple_library")
    .filter("mapEnabled", function () {
        return function (input) {
            if(input){
                return "enabled";
            }else{
                return "disabled";
            }
        }
    });