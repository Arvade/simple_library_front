angular.module("simple_library")
    .component("userPanel", {
        templateUrl: "simple_library/components/app/user-panel/user-panel.component.html",
        controller: "UserPanelController",
        controllerAs: "$ctrl"
    });