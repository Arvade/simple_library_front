angular.module("simple_library")
    .filter("mapRoles", function () {
        return function (role) {
            if (typeof(role) === "string") {
                return role;
            } else {
                return role[0].name;
            }
        }
    });