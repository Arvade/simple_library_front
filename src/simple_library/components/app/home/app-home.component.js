angular.module("simple_library")
    .component("appHome", {
        templateUrl: "simple_library/components/app/home/app-home.component.html",
        controller: "AppHomeController",
        controllerAs: "$ctrl",
        bindings:{
            books:"<"
        }
    });