angular.module("simple_library")
    .controller("AppHomeController", ["SessionService", function (SessionService) {
        var $ctrl = this;
        $ctrl.username = SessionService.getUsername();

    }]);