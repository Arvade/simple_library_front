angular.module("simple_library")
    .component("userPanelReadbooks", {
        templateUrl: "simple_library/components/app/user-panel-readbooks/user-panel-readbooks.component.html",
        controller: "UserPanelReadBooksController",
        controllerAs: "$ctrl",
        bindings:{
            readBooks:"<"
        }
    });