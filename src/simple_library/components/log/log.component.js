angular.module("simple_library")
    .component("log", {
        templateUrl: "simple_library/components/log/log.component.html",
        controller: "LogController",
        controllerAs: "$ctrl",
        bindings:{
            type:"<",
            content:"<"
        }
    });