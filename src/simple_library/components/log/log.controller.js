angular.module("simple_library")
    .controller("LogController", ["$timeout", function ($timeout) {
        var $ctrl = this;
        $ctrl.enabled = false;
        $ctrl.displayTime = 3000;

        $ctrl.$onInit = function () {
            $timeout($ctrl.showElement, 1000);
        };

        $ctrl.showElement = function () {
            $ctrl.enabled = true;
            $timeout($ctrl.hide, 2000);
        };

        $ctrl.hide = function () {
            $ctrl.enabled = false;
        };
    }]);