angular.module("simple_library")
    .component("home", {
        templateUrl: "simple_library/components/public/home/home.component.html",
        controller: "HomeController",
        controllerAs: "$ctrl",
        bindings:{
            books:"<"
        }
    });