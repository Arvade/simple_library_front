angular.module("simple_library")
    .component("public", {
        templateUrl: "simple_library/components/public/public.component.html",
        controller: "PublicController",
        controllerAs: "$ctrl"
    });