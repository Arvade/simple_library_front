angular.module("simple_library")
    .component("categories", {
        templateUrl: "simple_library/components/public/categories/categories.component.html",
        controller: "CategoriesController",
        controllerAs: "$ctrl",
        bindings: {
            categories: "<"
        }
    });