angular.module("simple_library")
    .controller("LoginController", ["$state", "$scope", "AuthService", "SessionService", "LoggingService",
        function ($state, $scope, AuthService, SessionService, LoggingService) {
            var $ctrl = this;
            $ctrl.submit = submit;
            $ctrl.reset = reset;

            $ctrl.defaultUser = {
                username: "",
                password: ""
            };

            $ctrl.user = {
                username: "",
                password: ""
            };

            function submit() {
                AuthService.login($ctrl.user)
                    .then(function (response) {
                        var authToken = response.token;
                        SessionService.setAuthToken(authToken);
                        $state.go("app.user.details");
                    }, function (data) {
                        logErrors(data.fieldErrors);
                        logErrors(data.globalErrors);
                    });
            }

            function logErrors(errors) {
                angular.forEach(errors, function (error) {
                    LoggingService.error(error.code);
                })
            }

            function reset() {
                $ctrl.user = $ctrl.defaultUser;
                $scope.loginForm.$setUntouched();
                $scope.loginForm.$setPristine();
            }
        }]);