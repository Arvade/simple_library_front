angular.module("simple_library")
    .component("login", {
        templateUrl: "simple_library/components/public/login/login.component.html",
        controller: "LoginController",
        controllerAs: "$ctrl"
    });