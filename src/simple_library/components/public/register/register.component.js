angular.module("simple_library")
    .component("register", {
        templateUrl: "simple_library/components/public/register/register.component.html",
        controller: "RegisterController",
        controllerAs: "$ctrl"
    });