angular.module("simple_library")
    .directive("emailExists", ["$http", "$q", "REST_BASEURL", "LoggingService", "UserService",
        function ($http, $q, REST_BASEURL, LoggingService, UserService) {
            return {
                require: "ngModel",
                link: function (scope, element, attrs, ngModel) {
                    ngModel.$asyncValidators.emailExists = function (modelValue, viewValue) {
                        if (viewValue == null || viewValue.length === 0) return;

                        var request = {
                            name: viewValue
                        };

                        return UserService.emailExists(request)
                            .then(function (data) {
                                var exists = data.exists;
                                if (exists) {
                                    return $q.reject();
                                }
                                return true;
                            });
                    }
                }
            }
        }]);