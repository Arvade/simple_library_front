angular.module("simple_library")
    .controller("RegisterController", ["$scope", "AuthService", "LoggingService", "$state",
        function ($scope, AuthService, LoggingService, $state) {
            var $ctrl = this;
            $ctrl.submit = submit;
            $ctrl.reset = reset;

            $ctrl.user = {
                username: "",
                password: "",
                email: "",
                firstname: "",
                lastname: ""
            };

            function submit() {
                AuthService.register($ctrl.user)
                    .then(function () {
                        LoggingService.success("Registration success!");
                        $state.go("public.login");
                    }, function () {
                        LoggingService.error("Could not register");
                    });
            }

            function reset() {
                $ctrl.user = {};
                $scope.loginForm.$setUntouched();
                $scope.loginForm.$setPristine();
            }
        }]);