angular.module("simple_library")
    .directive("usernameExists", ["$q", "$http", "REST_BASEURL", "UserService",
        function ($q, $http, REST_BASEURL, UserService) {
            return {
                require: "ngModel",
                link: function (scope, element, attrs, ngModel) {
                    ngModel.$asyncValidators.usernameExists = function (modelValue, viewValue) {
                        if (viewValue == null || viewValue.length === 0) return;
                        var request = {
                            name: viewValue
                        };

                        return UserService.usernameExists(request)
                            .then(function (data) {
                                var exists = data.exists;
                                if (exists) {
                                    return $q.reject();
                                }
                                return true;
                            })
                    };
                }
            }
        }]);