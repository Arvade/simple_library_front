angular.module("simple_library")
    .component("book", {
        templateUrl: "simple_library/components/book/book.component.html",
        controller: "BookController",
        controllerAs: "$ctrl",
        bindings: {
            data:"<"
        }
    });