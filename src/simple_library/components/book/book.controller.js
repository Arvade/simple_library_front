angular.module("simple_library")
    .controller("BookController", ["$state", function ($state) {
        var $ctrl = this;
        $ctrl.hasCoverImage = false;
        $ctrl.goToBookPanel = goToBookPanel;
        $ctrl.$onInit = onInit;

        function goToBookPanel() {
            $state.go("public.book.panel", {
                id: $ctrl.data.id
            })
        }

        function onInit() {
            $ctrl.hasCoverImage = ($ctrl.data.coverImageData != null);
            $ctrl.isRead = ($ctrl.data.read || false);
        }
    }]);