angular.module("simple_library")
    .controller("BookPanelController", ["BookService", "LoggingService", "FileService", "$sce",
        function (BookService, LoggingService, FileService, $sce) {
            var $ctrl = this;
            $ctrl.setBookAsRead = setBookAsRead;
            $ctrl.setBookAsUnread = setBookAsUnread;
            $ctrl.setBookAsToRead = setBookAsToRead;
            $ctrl.setBookAsNotToRead = setBookAsNotToRead;
            $ctrl.likeBook = likeBook;
            $ctrl.unlikeBook = unlikeBook;
            $ctrl.download = download;

            $ctrl.imageExists = false;
            $ctrl.downloading = false;

            $ctrl.$onInit = function () {
                var coverImageData = $ctrl.book.coverImageData;
                $ctrl.imageExists = (coverImageData != null && coverImageData !== undefined);
                if ($ctrl.imageExists) {
                    $ctrl.book.coverImageData = "data:image/png;base64," + coverImageData;
                }
            };

            function setBookAsRead() {
                BookService.setBookAsRead($ctrl.book.id)
                    .then(function () {
                        LoggingService.success("Book marked as read");
                        $ctrl.book.read = true;
                    }, function () {
                        LoggingService.error("Book was not marked. Something went wrong");
                    });
            }

            function setBookAsUnread() {
                BookService.setBookAsUnread($ctrl.book.id)
                    .then(function () {
                        LoggingService.success("Book is not longer marked as read");
                        $ctrl.book.read = false;
                    }, function () {
                        LoggingService.error("Could not unmark book");
                    })
            }

            function setBookAsToRead() {
                BookService.setBookAsToRead($ctrl.book.id)
                    .then(function () {
                        LoggingService.success("Book marked as toread");
                        $ctrl.book.todo = true;
                    }, function () {
                        LoggingService.error("Could not unmark book from toread.");
                    });
            }

            function setBookAsNotToRead() {
                BookService.setBookAsNotToRead($ctrl.book.id)
                    .then(function () {
                        LoggingService.success("Book is not longer marked toread");
                        $ctrl.book.todo = false;
                    }, function () {
                        LoggingService.error("Could unmark book from toread");
                    });
            }

            function likeBook() {
                BookService.likeBook($ctrl.book.id)
                    .then(function () {
                        LoggingService.success("Liked a book");
                        $ctrl.book.like = true;
                    }, function () {
                        LoggingService.error("Could not like a book");
                    });
            }

            function unlikeBook() {
                BookService.unlikeBook($ctrl.book.id)
                    .then(function () {
                        LoggingService.success("Book is no longer liked");
                        $ctrl.book.like = false;
                    }, function () {
                        LoggingService.error("Could not unlike book");
                    })
            }

            function download() {
                $ctrl.downloading = true;
                FileService.getFile($ctrl.book.contentId)
                    .then(function (data) {
                        var blob = new Blob([data], {type: 'application/pdf'});
                        var fileURL = URL.createObjectURL(blob);
                        var url = $sce.trustAsResourceUrl(fileURL);
                        var a = document.createElement('a');
                        a.href = url;
                        a.download = $ctrl.book.title + ".pdf";
                        a.click();
                        $ctrl.downloading = false;
                    }, function () {
                        $ctrl.downloading = false;
                        LoggingService.error("Could not download book");
                    })
            }
        }]);
