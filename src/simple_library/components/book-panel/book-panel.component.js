angular.module("simple_library")
    .component("bookPanel", {
        templateUrl: "simple_library/components/book-panel/book-panel.component.html",
        controller: "BookPanelController",
        controllerAs: "$ctrl",
        bindings: {
            book: "<",
            isLoggedIn:"<"
        }
    });