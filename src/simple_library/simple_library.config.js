angular.module("simple_library")
    .config(["$httpProvider", "$stateProvider", "$urlRouterProvider",
        function ($httpProvider, $stateProvider, $urlRouterProvider) {

            $stateProvider.state("public", {
                url: "/public",
                abstract: true,
                component: "public"
            }).state("public.home", {
                url: "/home",
                component: "home",
                resolve: {
                    books: ["BookService", function (BookService) {
                        return BookService.getBooksCovers()
                            .then(function (data) {
                                data = parseBookData(data);
                                decodeCoverImages(data);
                                return data;
                            });
                    }]
                }
            }).state("public.categories", {
                url: "/categories",
                component: "categories",
                resolve: {
                    categories: ["CategoryService", function (CategoryService) {
                        return CategoryService.getHomePageCategories()
                            .then(function (data) {
                                angular.forEach(data, function (record) {
                                    record.bookList = parseBookData(record.bookList);
                                });
                                return data;
                            });
                    }]
                }
            }).state("public.login", {
                url: "/login",
                component: "login"
            }).state("public.register", {
                url: "/register",
                component: "register"
            }).state("public.book", {
                url: "/books",
                abstract: true
            }).state("public.book.panel", {
                url: "/:id",
                component: "bookPanel",
                resolve: {
                    book: ["$stateParams", "BookService", "SessionService",
                        function ($stateParams, BookService, SessionService) {
                            var id = $stateParams.id;

                            if (SessionService.isLoggedIn()) {
                                return BookService.getBookDetails(id)
                                    .then(function (data) {
                                        return data;
                                    });
                            } else {
                                return BookService.getBookById(id)
                                    .then(function (data) {
                                        return data;
                                    });
                            }

                        }],
                    isLoggedIn: ["SessionService", function (SessionService) {
                        return SessionService.isLoggedIn();
                    }]
                }
            });


            $stateProvider.state("app", {
                url: "/app",
                abstract: true,
                component: "app",
                onEnter: ["$state", "SessionService", "LoggingService",
                    function ($state, SessionService, LoggingService) {
                        var isLoggedIn = SessionService.isLoggedIn();
                        if (!isLoggedIn) {
                            $state.go("public.login");
                            LoggingService.warn("Your session expired. Please log in.");
                        }
                    }]
            }).state("app.home", {
                url: "/home",
                component: "appHome",
                resolve: {
                    books: ["BookService", function (BookService) {
                        return BookService.getAllBooksWithDetails()
                            .then(function (data) {
                                decodeCoverImages(data);
                                return data;
                            });
                    }]
                }
            }).state("app.user", {
                url: "/user",
                abstract: true,
                component: "userPanel"
            }).state("app.user.details", {
                url: "/info",
                component: "userPanelInfo",
                resolve: {
                    userInfo: ["UserService", function (UserService) {
                        var username = UserService.getLoggerInUsername();
                        return UserService.getUserInfo(username)
                            .then(function (data) {
                                return data;
                            });
                    }]
                }
            }).state("app.user.readBooks", {
                url: "/read",
                component: "userPanelReadbooks",
                resolve: {
                    readBooks: ["BookService", function (BookService) {
                        return BookService.getReadBooks()
                            .then(function (data) {
                                data = parseBookData(data);
                                decodeCoverImages(data);
                                return data;
                            });
                    }]
                }
            }).state("app.user.todoBooks", {
                url: "/todoBooks",
                component: "userPanelBookstoread",
                resolve: {
                    toReadBooks: ["BookService", function (BookService) {
                        return BookService.getToReadBooks()
                            .then(function (data) {
                                data = parseBookData(data);
                                decodeCoverImages(data);
                                return data;
                            })
                    }]
                }
            }).state("app.user.likedBooks", {
                url: "/likedBooks",
                component: "userPanelLikedbooks",
                resolve: {
                    likedBooks: ["BookService", function (BookService) {
                        return BookService.getLikedBooks()
                            .then(function (data) {
                                data = parseBookData(data);
                                decodeCoverImages(data);
                                return data;
                            })
                    }]
                }
            }).state("app.user.userList", {
                url: "/list",
                component: "userPanelUserlist"
            }).state("app.user.addBook", {
                url: "/addBook",
                component: "userPanelAddbook",
                resolve: {
                    categories: ["CategoryService", "LoggingService",
                        function (CategoryService, LoggingService) {
                            return CategoryService.getAllBookCategories()
                                .then(function (data) {
                                    LoggingService.success("Book categories downloaded!");
                                    return data;
                                })
                        }]
                }
            }).state("app.user.addCategory", {
                url: "/addCategory",
                component: "userPanelAddcategory"
            }).state("app.user.categoryList", {
                url: "/categoryList",
                component: "userPanelCategorylist"
            });

            function parseBookData(data) {
                var resultValue = [];
                angular.forEach(data, function (record) {
                    var obj = {};
                    obj.id = record.bookId;
                    obj.title = record.bookTitle;
                    obj.coverImageData = record.bookCoverImageData;
                    resultValue.push(obj);
                });
                return resultValue;
            }

            function decodeCoverImages(data) {
                angular.forEach(data, function (record) {
                    if (record.coverImageData != null && record.coverImageData !== undefined) {
                        record.coverImageData = "data:image/png;base64," + record.coverImageData;
                    }
                });
            }

            $urlRouterProvider.otherwise("/public/home");
            $httpProvider.interceptors.push("ErrorInterceptor");
            $httpProvider.interceptors.push("AuthInterceptor");
        }]);