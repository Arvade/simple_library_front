angular.module("simple_library")
    .factory("AuthInterceptor", ["SessionService", "REST_BASEURL", "$state", "LoggingService",
        function (SessionService, REST_BASEURL, $state, LoggingService) {
            return {
                request: function (config) {
                    if (config.url.indexOf(REST_BASEURL) === -1) {
                        return config;
                    }
                    config.headers = config.headers || {};
                    if (SessionService.isLoggedIn()) {
                        config.headers.Authorization = "Bearer " + SessionService.getAuthToken();
                    }
                    return config;
                },
                responseError: function (response) {
                    if (response.config.url.indexOf(REST_BASEURL) === -1) {
                        return response;
                    }
                    if (response.status === 401) {
                        SessionService.logout();
                        $state.go("public.login");
                        LoggingService.warn("Your session expired. Please log in");
                    }

                    return response;
                }
            }
        }]);