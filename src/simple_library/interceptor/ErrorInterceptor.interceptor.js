angular.module("simple_library")
    .factory("ErrorInterceptor", ["$q", "LoggingService", "REST_BASEURL",
        function ($q, LoggingService, REST_BASEURL) {
            return {
                response: function (response) {
                    if (response.config.url.indexOf(REST_BASEURL) === -1) {
                        return response;
                    }
                    if (response.status === 403) {
                        LoggingService.warn("You don't have permission to do that");
                    }

                    if (response.status === 500) {
                        LoggingService.error("Something went wrong");
                    }

                    if (response.status !== 200) {
                        return $q.reject(response);
                    }

                    return response;
                }
            }
        }]);