angular.module("simple_library",
    ["ui.router", "ngMessages", "ngStorage", "ngAnimate", "ui.grid", "ui.grid.pagination",
        "ui.grid.edit", "ng-bootstrap-select", "ngFileUpload"]);