angular.module("simple_library")
    .service("LoggingService", ["$rootScope", function ($rootScope) {
        return {
            success:success,
            warn:warn,
            error:error
        };

        function success(content){
            logInternal("alert-success", content);
        }

        function warn(content){
            logInternal("alert-warning", content);
        }

        function error(content){
            logInternal("alert-danger", content);
        }

        function logInternal(type, content){
            var log = {
                type:type,
                content:content
            };
            $rootScope.$emit("LoggingService:log-added", log);
        }
    }]);