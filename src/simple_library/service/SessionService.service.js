angular.module("simple_library")
    .service("SessionService", function ($sessionStorage, $rootScope) {
        return {
            setAuthToken: setAuthToken,
            getAuthToken: getAuthToken,
            isLoggedIn: isLoggedIn,
            logout: logout,
            getUsername: getUsername
        };

        function setAuthToken(authToken) {
            $sessionStorage.authToken = authToken;
            $rootScope.$emit("SessionService:logged");
        }

        function getAuthToken() {
            return $sessionStorage.authToken;
        }

        function isLoggedIn() {
            var authToken = getAuthToken();
            if (authToken == null || authToken.length === 0) return false;
            var parts = authToken.split(".");
            return (parts.length === 3);
        }

        function logout() {
            setAuthToken(null);
            $rootScope.$emit("SessionService:logout");
        }

        function getUsername() {
            if (!isLoggedIn()) return null;
            var authToken = getAuthToken();
            var encodedClaims = authToken.toString().split(".")[1];
            var claims = JSON.parse(atob(encodedClaims));

            return claims.sub;
        }
    });