angular.module("simple_library")
    .service("CategoryService", ["$q", "$http", "REST_BASEURL",
        function ($q, $http, REST_BASEURL) {
            return {
                getHomePageCategories: getHomePageCategories,
                addCategory: addCategory,
                categoryExists: categoryExists,
                updateCategory: updateCategory,
                removeCategory: removeCategory,
                getBookCategoriesPaginated: getBookCategoriesPaginated,
                getAllBookCategories: getAllBookCategories,
            };

            function getHomePageCategories() {
                return $http.get(REST_BASEURL + "/categories")
                    .then(successCallback, errorCallback);
            }

            function addCategory(category) {
                return $http.post(REST_BASEURL + "/app/admin/category/create", category)
                    .then(successCallback, errorCallback);
            }

            function categoryExists(category) {
                return $http.post(REST_BASEURL + "/app/admin/category/exists", category)
                    .then(successCallback, errorCallback);
            }

            function updateCategory(updateRequest) {
                return $http.post(REST_BASEURL + "/app/admin/category/update", updateRequest)
                    .then(successCallback, errorCallback);
            }

            function removeCategory(deleteRequest) {
                return $http.post(REST_BASEURL + "/app/admin/category/delete", deleteRequest)
                    .then(successCallback, errorCallback);
            }

            function getAllBookCategories() {
                return $http.get(REST_BASEURL + "/books/categories")
                    .then(successCallback, errorCallback);
            }

            function getBookCategoriesPaginated(page) {
                page.page = (page.page > 0) ? page.page - 1 : page.page;
                return $http.get(REST_BASEURL + "/app/admin/categories/" + page.page + "/" + page.size)
                    .then(successCallback, errorCallback);
            }

            function successCallback(response) {
                return response.data;
            }

            function errorCallback(response) {
                return $q.reject(response);
            }
        }]);