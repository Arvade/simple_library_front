angular.module("simple_library")
    .service("BookService", ["$http", "$q", "REST_BASEURL",
        function ($http, $q, REST_BASEURL) {
            return {
                getBooksCovers: getBooksCovers,
                getBookByTitle: getBookByTitle,
                getBookById: getBookById,
                getAllBooksWithDetails: getAllBooksWithDetails,
                getBookDetails: getBookDetails,
                setBookAsToRead: setBookAsToRead,
                setBookAsNotToRead: setBookAsNotToRead,
                setBookAsRead: setBookAsRead,
                setBookAsUnread: setBookAsUnread,
                likeBook: likeBook,
                unlikeBook: unlikeBook,
                getLikedBooks: getLikedBooks,
                getReadBooks: getReadBooks,
                getToReadBooks: getToReadBooks,
                addBook: addBook,
            };

            function getBooksCovers() {
                return $http.get(REST_BASEURL + "/books/home")
                    .then(successCallback, errorCallback);
            }

            function getBookByTitle(title) {
                return $http.get(REST_BASEURL + "/books/" + title)
                    .then(successCallback, errorCallback);
            }

            function getBookById(id) {
                return $http.get(REST_BASEURL + "/books/" + id)
                    .then(successCallback, errorCallback);
            }

            function setBookAsRead(bookId) {
                return $http.get(REST_BASEURL + "/app/books/markAsRead/" + bookId)
                    .then(successCallback, errorCallback);
            }

            function setBookAsUnread(bookId) {
                return $http.delete(REST_BASEURL + "/app/books/markAsRead/" + bookId)
                    .then(successCallback, errorCallback);
            }

            function getAllBooksWithDetails() {
                return $http.get(REST_BASEURL + "/app/books")
                    .then(successCallback, errorCallback);
            }

            function getBookDetails(bookId) {
                return $http.get(REST_BASEURL + "/app/books/" + bookId)
                    .then(successCallback, errorCallback);
            }

            function setBookAsToRead(bookId) {
                return $http.get(REST_BASEURL + "/app/books/markAsToRead/" + bookId)
                    .then(successCallback, errorCallback);
            }

            function setBookAsNotToRead(bookId) {
                return $http.delete(REST_BASEURL + "/app/books/markAsToRead/" + bookId)
                    .then(successCallback, errorCallback);
            }

            function likeBook(bookId) {
                return $http.get(REST_BASEURL + "/app/books/like/" + bookId)
                    .then(successCallback, errorCallback);
            }

            function unlikeBook(bookId) {
                return $http.delete(REST_BASEURL + "/app/books/like/" + bookId)
                    .then(successCallback, errorCallback);
            }

            function getReadBooks() {
                return $http.get(REST_BASEURL + "/app/books/readBooks")
                    .then(successCallback, errorCallback);
            }

            function getToReadBooks() {
                return $http.get(REST_BASEURL + "/app/books/toRead")
                    .then(successCallback, errorCallback);
            }

            function getLikedBooks() {
                return $http.get(REST_BASEURL + "/app/books/like")
                    .then(successCallback, errorCallback);
            }

            function addBook(book) {
                return $http.post(REST_BASEURL + "/app/admin/books/add", book)
                    .then(successCallback, errorCallback);
            }

            function successCallback(response) {
                return response.data;
            }

            function errorCallback(response) {
                return $q.reject(response);
            }
        }]);