angular.module("simple_library")
    .service("UserService", ["$http", "$q", "REST_BASEURL", "SessionService",
        function ($http, $q, REST_BASEURL, SessionService) {
            return {
                getLoggerInUsername: getLoggedInUsername,
                getUserInfo: getUserInfo,
                getUserRoles: getUserRoles,
                isAdmin: isAdmin,
                getUserList: getUserList,
                updateUser: updateUser,
                usernameExists: usernameExists,
                emailExists: emailExists
            };

            function getLoggedInUsername() {
                var token = SessionService.getAuthToken();
                var encodedClaims = extractClaims(token);
                var decodedClaims = JSON.parse(atob(encodedClaims));

                return decodedClaims.sub;
            }

            function getUserInfo(username) {
                return $http.get(REST_BASEURL + "/user/" + username)
                    .then(function (response) {
                        return response.data;
                    }, function (response) {
                        return $q.reject(response.data);
                    })
            }

            function getUserRoles() {
                var token = SessionService.getAuthToken();
                var encodedClaims = extractClaims(token);
                var decodedClaims = JSON.parse(atob(encodedClaims));

                return decodedClaims.role;
            }

            function isAdmin() {
                var token = SessionService.getAuthToken();
                var roles = extractRoles(token);
                return (roles.indexOf("ROLE_ADMIN") !== -1);
            }

            function getUserList(pageRequest) {
                var page = pageRequest.page;
                var size = pageRequest.size;
                page = (page > 0) ? page - 1 : 0;
                return $http.get(REST_BASEURL + "/app/admin/users/" + page + "/" + size)
                    .then(function (response) {
                        return response.data;
                    }, function (response) {
                        return $q.reject(response);
                    });
            }

            function updateUser(updateRequest) {
                return $http.post(REST_BASEURL + "/app/admin/user/edit", updateRequest)
                    .then(function (response) {
                        return response.data;
                    }, function (response) {
                        return $q.reject(response);
                    })
            }

            function usernameExists(request) {
                return $http.post(REST_BASEURL + "/user/username/exists", request)
                    .then(function (response) {
                        return response.data;
                    }, function (response) {
                        return $q.reject(response);
                    })
            }

            function emailExists(request) {
                return $http.post(REST_BASEURL + "/user/email/exists", request)
                    .then(function (response) {
                        return response.data;
                    }, function (response) {
                        return $q.reject(response);
                    })
            }

            function extractRoles(token) {
                var encodedClaims = extractClaims(token);
                var decodedClaims = JSON.parse(atob(encodedClaims));

                var rolesArray = decodedClaims.role;
                var returnValue = [];
                for (var i = 0; i < rolesArray.length; i++) {
                    var role = rolesArray[i];
                    returnValue.push(role.authority);
                }
                return returnValue;
            }

            function extractClaims(token) {
                var tokenParts = token.split(".");
                return tokenParts[1];
            }
        }]);