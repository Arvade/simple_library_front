angular.module("simple_library")
    .service("FileService", ["$q", "$http", "REST_BASEURL", "Upload",
        function ($q, $http, REST_BASEURL, Upload) {
            return {
                uploadFile: uploadFile,
                getFile: getFile
            };

            function uploadFile(file) {
                return Upload.upload({
                    url: REST_BASEURL + "/app/admin/uploadFile",
                    file: file
                }).then(successCallback, errorCallback);
            }

            function getFile(id) {
                var request = {
                    id: id
                };

                var config = {
                    responseType: "arraybuffer"
                };

                return $http.post(REST_BASEURL + "/app/file/", request, config)
                    .then(successCallback, errorCallback);
            }


            function successCallback(response) {
                return response.data;
            }

            function errorCallback(response) {
                return $q.reject(response);
            }
        }]);