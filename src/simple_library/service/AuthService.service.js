angular.module("simple_library")
    .service("AuthService", ["$http", "$q", "REST_BASEURL",
        function ($http, $q, REST_BASEURL) {
            var self = this;
            self.register = register;
            self.login = login;

            function register(user) {
                return $http.post(REST_BASEURL + "/register", user)
                    .then(function (response) {
                        return response.data;
                    }, function (response) {
                        return $q.reject(response.data);
                    });
            }

            function login(user) {
                return $http.post(REST_BASEURL + "/auth", user)
                    .then(function (response) {
                        return response.data;
                    }, function (response) {
                        return $q.reject(response.data);
                    })
            }
        }]);